# mips-toolchain
Needs to be run from an older Linux distribution.  I created an Ubuntu Lucid Lynx VM that works fine.  Simply execute the script in your VM using `$ ./cross-compiler-bootstrap.sh`.

## References
* https://www.held-im-ruhestand.de/software/embedded/cross-compiler.html
* https://github.com/Xilinx/eglibc/blob/master/EGLIBC.cross-building
