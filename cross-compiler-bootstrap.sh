#!/bin/bash
set -e
set -u

# building my custom cross compiler

# This directory is used as root for all operations
BASE=$(dirname "$0")
BASE=$(cd "$BASE" && pwd)

# target machine tuple. This is suitable for the marvell sheevaplug
#TARGET=armv5tel-softfloat-linux-gnueabi
TARGET=mipsel-softfloat-linux-gnueabi
BUILD=$(gcc -dumpmachine)        # x86_64-unknown-linux-gnu, i686-linux-gnu, ...
#ARCH=arm
ARCH=mips

SYSROOT="$BASE/sysroot"     # files for the target system 
TOOLCHAIN="$BASE/toolchain" # the cross compilers
SRCDIR="$BASE/src"          # saving tarballs
WORKDIR="$BASE/work"        # unpacked tarballs
BUILDDIR="$BASE/build"      # running compile 

PARALLEL="-j2"              # dualcore

# Software Versions to use
#BINUTILS=binutils-2.20.1
#KERNEL=linux-2.6.34
#GCC=gcc-4.5.0
#EGLIBC=eglibc-2_11          # abi compatible to glibc, better support for arm

#changes: 
#  versions below
BINUTILS=binutils-2.22
KERNEL=linux-3.0.8
#KERNEL=linux-2.6.38
GCC=gcc-4.5.2
EGLIBC=eglibc-2_13          # abi compatible to glibc, better support for arm
# changes:
#   needed to also include gmp-5.0.1, mpc-1.0.1, and mpfr-3.0.0 for gcc-4.5.2 to build
#   gmp needed a symbolic link created in the mpn folder 'ln -s ../mpn mpn'
#   added --disable-werror on all the calls to configure below

# download functions
get_url() {
	# pass a full url as parameter
	url=$1
	file=${url##*/}
	if [ ! -f "$SRCDIR/$file" ]; then
		echo "downloading $file from $url"
		wget "$url" -O "$BASE/src/$file"
	fi
}

unpack() {
	# pass a filename as parameter
	file=$1
	ext=${file##*.}
	folder=${file%%.tar.*}
	if [ !  -d "$WORKDIR/$folder"  ]; then
		echo "unpacking $file"
	else
		return 0
	fi
	cd $WORKDIR
	if [ $ext == "bz2" ]; then
		tar jxf "$SRCDIR/$file"
	else
		tar zxf "$SRCDIR/$file"
	fi
}

check_done() {
	OBJ=$1
	if [ -f $BUILDDIR/$OBJ.done ]; then
		echo "already done"
		return 0
	fi
	return 1
}

do_msg() {
	OBJ=$1
	ACTION=$2
	echo "========================================================="
	echo "$OBJ - $ACTION"
	echo "========================================================="
}


binutils() {
	# compile ar, as, ld, objcopy, ...
	OBJ=$BINUTILS
	do_msg $OBJ "start"
	check_done $OBJ && return 0
	mkdir -p $BUILDDIR/$OBJ
	pushd $BUILDDIR/$OBJ
	if [ ! -f Makefile ]; then
		# run configure
		$WORKDIR/$OBJ/configure \
			--target=$TARGET \
			--prefix=$TOOLCHAIN \
			--with-sysroot=$SYSROOT \
			--disable-nls \
			--disable-werror
	fi
	do_msg $OBJ "compile"
	make $PARALLEL
	do_msg $OBJ "install"
	make install 
	do_msg $OBJ "done"
	touch $BUILDDIR/$OBJ.done
	popd
}


gccstatic() {
	# static gcc, only C, able to compile the libc
	# would be enough if we only compile kernels
	OBJ=$GCC-static
	do_msg $OBJ "start"
	check_done $OBJ && return 0
	mkdir -p $BUILDDIR/$OBJ
	pushd $BUILDDIR/$OBJ
	if [ ! -f Makefile ]; then
		$WORKDIR/$GCC/configure \
			--target=$TARGET \
			--prefix=$TOOLCHAIN \
			--without-headers \
			--with-newlib \
			--disable-shared \
			--disable-threads \
			--disable-libssp \
			--disable-libgomp \
			--disable-libmudflap \
			--disable-nls \
			--enable-languages=c \
			--disable-werror
	# --without-headers and --with-newlib make gcc believe that there
	# is no c library available (yet)
	fi
	do_msg $OBJ "compile"
	PATH=$TOOLCHAIN/bin:$PATH \
	make $PARALLEL 
	do_msg $OBJ "install"
	PATH=$TOOLCHAIN/bin:$PATH \
	make install
	do_msg $OBJ "done"
	touch $BUILDDIR/$OBJ.done
	popd
}

gccminimal() {
	OBJ=$GCC-min
	do_msg $OBJ "start"
	check_done $OBJ && return 0
	mkdir -p $BUILDDIR/$OBJ
	pushd $BUILDDIR/$OBJ
	if [ ! -f Makefile ]; then
		$WORKDIR/$GCC/configure \
			--target=$TARGET \
			--prefix=$TOOLCHAIN \
			--with-sysroot=$SYSROOT \
			--disable-libssp \
			--disable-libgomp \
			--disable-libmudflap \
			--disable-nls \
			--enable-languages=c \
			--disable-werror
	# now with shared libs and threads
	fi
	do_msg $OBJ "compile"
	PATH=$TOOLCHAIN/bin:$PATH \
	make $PARALLEL 
	do_msg $OBJ "install"
	PATH=$TOOLCHAIN/bin:$PATH \
	make install
	do_msg $OBJ "done"
	touch $BUILDDIR/$OBJ.done
	popd
}
gccfull() {
	OBJ=$GCC
	do_msg $OBJ "start"
	check_done $OBJ && return 0
	mkdir -p $BUILDDIR/$OBJ
	pushd $BUILDDIR/$OBJ
	if [ ! -f Makefile ]; then
		$WORKDIR/$GCC/configure \
			--target=$TARGET \
			--prefix=$TOOLCHAIN \
			--with-sysroot=$SYSROOT \
			--enable-__cxy_atexit \
			--disable-libssp \
			--disable-libgomp \
			--disable-libmudflap \
			--enable-languages=c \
			--disable-nls \
			--disable-werror
	# now with c++
	# the cxy_atexit is special for eglibc
	fi
	do_msg $OBJ "compile"
	PATH=$TOOLCHAIN/bin:$PATH \
	make $PARALLEL 
	do_msg $OBJ "install"
	PATH=$TOOLCHAIN/bin:$PATH \
	make install
	do_msg $OBJ "done"
	touch $BUILDDIR/$OBJ.done
	popd
}

kernelheader() {
	# compiling headers that work on the target system
	# this is done in-tree 
	OBJ=$KERNEL
	do_msg $OBJ "start"
	check_done $OBJ && return 0
	pushd $WORKDIR/$OBJ
	do_msg $OBJ "cleaning"
	make mrproper
	do_msg $OBJ "installing"
	PATH=$TOOLCHAIN/bin:$PATH \
	make \
		ARCH=$ARCH \
		INSTALL_HDR_PATH=$SYSROOT/usr \
		CROSS_COMPILE=$TARGET- \
		headers_install 
	do_msg $OBJ "done"
	touch $BUILDDIR/$OBJ.done
	popd
}

eglibcheader() {
	OBJ=$EGLIBC-header
	do_msg $OBJ "start"
	check_done $OBJ && return 0
	mkdir -p $BUILDDIR/$OBJ
	pushd $BUILDDIR/$OBJ
	if [ ! -f Makefile ]; then
		BUILD_CC=gcc \
		CC=$TOOLCHAIN/bin/$TARGET-gcc \
		CXX=$TOOLCHAIN/bin/$TARGET-g++ \
		AR=$TOOLCHAIN/bin/$TARGET-ar \
		LD=$TOOLCHAIN/bin/$TARGET-ld \
		RANLIB=$TOOLCHAIN/bin/$TARGET-ranlib \
		$WORKDIR/$EGLIBC/configure \
			--prefix=/usr \
			--with-headers="$SYSROOT/usr/include" \
			--build=$BUILD \
			--host=$TARGET \
			--disable-nls \
			--disable-profile \
			--without-gd \
			--without-cvs \
			--enable-add-ons \
			--disable-werror

	fi
	do_msg $OBJ "install"
	make \
		install-headers \
		install_root=$SYSROOT \
		install-bootstrap-headers=yes
	do_msg $OBJ "crtX and fake libc"
	make csu/subdir_lib
	mkdir -p $SYSROOT/usr/lib
	cp csu/crt1.o csu/crti.o csu/crtn.o $SYSROOT/usr/lib
	# build a dummy libc
	$TOOLCHAIN/bin/$TARGET-gcc \
		-nostdlib \
		-nostartfiles \
		-shared \
		-x c /dev/null \
		-o $SYSROOT/usr/lib/libc.so
	do_msg $OBJ "done"
	touch $BUILDDIR/$OBJ.done
	popd
}
eglibc() {
	OBJ=$EGLIBC
	do_msg $OBJ "start"
	check_done $OBJ && return 0
	mkdir -p $BUILDDIR/$OBJ
	pushd  $BUILDDIR/$OBJ
	if [ ! -f Makefile ]; then
#		CFLAGS="-O2 -mips32r2" \
		BUILD_CC=gcc \
		CC=$TOOLCHAIN/bin/$TARGET-gcc \
		CXX=$TOOLCHAIN/bin/$TARGET-g++ \
		AR=$TOOLCHAIN/bin/$TARGET-ar \
		RANLIB=$TOOLCHAIN/bin/$TARGET-ranlib \
		$WORKDIR/$EGLIBC/configure \
			--prefix=/usr \
			--with-headers="$SYSROOT/usr/include" \
			--build=$BUILD \
			--host=$TARGET \
			--disable-nls \
			--disable-profile \
			--without-gd \
			--without-cvs \
			--enable-add-ons \
			--disable-werror
	fi
	do_msg $OBJ "compile"
	PATH=$TOOLCHAIN/bin:$PATH \
	make $PARALLEL
	do_msg $OBJ "install"
	PATH=$TOOLCHAIN/bin:$PATH \
	make install install_root=$SYSROOT
	do_msg $OBJ "done"
	touch $BUILDDIR/$OBJ.done
	popd
}

# ==============================================
# MAIN
# ==============================================


do_msg "INIT" "creating directories"
mkdir -p $BUILDDIR
mkdir -p $WORKDIR
mkdir -p $SRCDIR
mkdir -p $SYSROOT
mkdir -p $TOOLCHAIN

do_msg "INIT" "download"
pushd $SRCDIR
get_url "http://ftp.gnu.org/gnu/binutils/$BINUTILS.tar.bz2"
get_url "ftp://ftp.gnu.org/gnu/gcc/$GCC/$GCC.tar.bz2"
get_url "http://mirrors.edge.kernel.org/pub/linux/kernel/v3.0/$KERNEL.tar.bz2"
#get_url "http://mirrors.edge.kernel.org/pub/linux/kernel/v2.6/$KERNEL.tar.bz2"
get_url "http://ftp.gnu.org/gnu/gmp/gmp-5.0.1.tar.bz2"
get_url "http://ftp.gnu.org/gnu/mpc/mpc-1.0.1.tar.gz"
get_url "http://ftp.gnu.org/gnu/mpfr/mpfr-3.0.0.tar.bz2"

# our very special friend eglibc has no release tarball
if [ ! -d $SRCDIR/$EGLIBC ]; then
	svn export svn://svn.eglibc.org/branches/$EGLIBC/libc $SRCDIR/$EGLIBC
	# arm is only supported in ports
	svn export svn://svn.eglibc.org/branches/$EGLIBC/ports $SRCDIR/$EGLIBC/ports
	svn export svn://svn.eglibc.org/branches/$EGLIBC/linuxthreads/linuxthreads $SRCDIR/$EGLIBC/linuxthreads
fi
	
do_msg "INIT" "unpacking"
unpack "$BINUTILS.tar.bz2"
unpack "$GCC.tar.bz2"
unpack "$KERNEL.tar.bz2"
unpack "gmp-5.0.1.tar.bz2"
unpack "mpc-1.0.1.tar.gz"
unpack "mpfr-3.0.0.tar.bz2"

temp=$WORKDIR/$GCC/gmp
if test -f "$temp"; then
	echo "$temp exists."
else
	ln -s $WORKDIR/gmp-5.0.1 $WORKDIR/$GCC/gmp
fi
temp=$WORKDIR/$GCC/mpc
if test -f "$temp"; then
	echo "$temp exists."
else
	ln -s $WORKDIR/mpc-1.0.1 $WORKDIR/$GCC/mpc
fi
temp=$WORKDIR/$GCC/mpfr
if test -f "$temp"; then
	echo "$temp exists."
else
	ln -s $WORKDIR/mpfr-3.0.0 $WORKDIR/$GCC/mpfr
fi
if [ ! -h $WORKDIR/$EGLIBC ]; then
	ln -s $SRCDIR/$EGLIBC $WORKDIR/$EGLIBC
fi

binutils
gccstatic
kernelheader
eglibcheader
gccminimal
eglibc
gccfull

echo "     _                   "
echo "  __| | ___  _ __   ___  "
echo " / _  |/ _ \| '_ \ / _ \ "
echo "| (_| | (_) | | | |  __/ "
echo " \__,_|\___/|_| |_|\___| "
echo "                         "
echo "                         "
