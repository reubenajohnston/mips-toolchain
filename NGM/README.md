Needs:
* NGM
* Trendnet TU2-ET100 usb to ethernet adapter
* USBC male to USBA female adapter
* *.fw file
* ADFU flashing tool
* modified SYSTEM (actb) partition
* built mips-toolchain
* cross-compiled lrzsz

Approach:
* dump the stock SYSTEM (actb) partition over UART (see my `https://gitlab.com/reubenajohnston/uartdownloader`)
* modify it by:
    * replacing `/usr/local/etc/dmenu/dmenu_ln` with this `dmenu_ln` file
    * creating a `/usr/local/etc/network` folder and inserting this `interfaces` file
    * inserting this `sysctl.conf` file in `/usr/local/etc/dmenu/dmenu_ln`
* flash the updated SYSTEM
* insert the TU2-ET100
* boot the NGM, UART login is: root, (empty)
* identify the ip address of the NGM by looking at the devices attached to your router
* via UART, `# ifconfig eth0 IPADDR` (this step will be eliminated at some point)
* via UART, `# telnetd -F -l /bin/sh` (this step will be eliminated at some point)
* from your development PC on the same network, `$ telnet IPADDRESS`
