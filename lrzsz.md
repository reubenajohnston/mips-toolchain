# Instructions for building lrzsz
* Download the lrzsz source code from [here](https://www.ohse.de/uwe/software/lrzsz.html)
* Add the following lines to the top of the lrzsz project's `configure` file:
```
SYSROOT=/PATHTOYOUR/sysroot
CFLAGS="-mips32r2 -I${SYSROOT}/usr/include"
LDFLAGS="--sysroot=${SYSROOT}"
ARCH=mips
TARGET=mipsel-softfloat-linux-gnueabi
TOOLCHAIN_DIR=/PATHTOYOUR/build/cross-toolchain/toolchain/bin
PATH=$TOOLCHAIN_DIR/bin:$PATH
CROSS_COMPILE=$TARGET-
BUILD=$(gcc -dumpmachine)
CC=${CROSS_COMPILE}gcc
```
* Run configure using:
    `$ PATH=/PATHTOYOUR/build/cross-toolchain/toolchain/bin:$PATH ./configure`
* Make the binaries using: 
    `$ PATH=/PATHTOYOUR/build/cross-toolchain/toolchain/bin:$PATH make`
* `lrz` and `lsz` binaries will be located in the src folder
    * for minicom, you can upload these files with rx from the target
    * e.g., on the target, `$ rx lrz`, then press `ctrl-a` and next `s`, select xmodem, select the file and press enter
    * then, you will see the xmodem upload send the file over to the target
